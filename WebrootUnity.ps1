#region -[Authentication]----------------------------------------------

function Get-WebrootAuthToken {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/PasswordAuthentication
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$client_id,
        [Parameter(Mandatory=$True)]
        [string]$client_secret,
        [string]$username,
        [string]$password,
        [string]$scope = '*'    
    )

    $url = 'https://unityapi.webrootcloudav.com/auth/token'

    if(!$username -or !$password){
        Write-Verbose "Credentials not passed, prompting."
        $Credentials = Get-Credential -Message "Webroot SecureAnywhere GSM" -UserName $username
        $username = $Credentials.UserName
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Credentials.Password)
        $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    }

    $Body = @{username=$username;
              password=$password;
              grant_type='password';
              scope=$scope}

    $Auth = "$($client_id):$client_secret"
    $Bytes = [System.Text.Encoding]::ASCII.GetBytes($Auth)
    $Auth =[Convert]::ToBase64String($Bytes)

    try{
        Write-Verbose "Getting auth token."
        $Obj = Invoke-RestMethod -Method Post -Uri $url -Body $Body -ContentType "application/x-www-form-urlencoded" -Headers @{Authorization = "Basic $Auth"}
        $Obj | Add-Member -NotePropertyName 'client_id' -NotePropertyValue $client_id
        $Obj | Add-Member -NotePropertyName 'client_secret' -NotePropertyValue $client_secret
        $Obj | Add-Member -NotePropertyName 'expires' -NotePropertyValue (Get-Date).AddSeconds(295)
        $Obj | Add-Member -NotePropertyName 'Renewable' -NotePropertyValue (Get-Date).AddDays(14).AddSeconds(-5)
        $global:WebrootAuthToken = $Obj | Select * -ExcludeProperty 'expires_in'
        Write-Verbose 'Auth Token saved to $WebrootAuthToken'
    }
    catch{
        Write-Error "ERROR : Please reffer to website for Status Codes. https://unityapi.webrootcloudav.com/Docs/APIDoc/Guide#guide-statusCodes `r`n $_"
    }
}

function Update-WebrootAuthToken {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/RefreshTokenAuthentication
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$client_id,
        [Parameter(Mandatory=$True)]
        [string]$client_secret,        
        [string]$refresh_token = $WebrootAuthToken.refresh_token,
        [string]$scope = '*'    
    )

    $url = 'https://unityapi.webrootcloudav.com/auth/token'

    $Body = @{refresh_token=$refresh_token;
              grant_type='refresh_token';
              scope=$scope}
    
    $Text = "$($client_id):$client_secret"
    $Bytes = [System.Text.Encoding]::ASCII.GetBytes($Text)
    $EncodedText =[Convert]::ToBase64String($Bytes)

    try{
        Write-Verbose "Renewing auth token."
        $Obj = Invoke-RestMethod -Method Post -Uri $url -Body $Body -ContentType "application/x-www-form-urlencoded" -Headers @{Authorization = "Basic $EncodedText"}
        $Obj | Add-Member -NotePropertyName 'client_id' -NotePropertyValue $client_id
        $Obj | Add-Member -NotePropertyName 'client_secret' -NotePropertyValue $client_secret
        $Obj | Add-Member -NotePropertyName 'expires' -NotePropertyValue (Get-Date).AddSeconds(295)
        $Obj | Add-Member -NotePropertyName 'Renewable' -NotePropertyValue (Get-Date).AddDays(14).AddSeconds(-5)
        $global:WebrootAuthToken = $Obj | Select * -ExcludeProperty 'expires_in'
        Write-Verbose 'Auth Token saved to $WebrootAuthToken'
    }
    catch{
        Write-Error "ERROR : Please reffer to website for Status Codes. https://unityapi.webrootcloudav.com/Docs/APIDoc/Guide#guide-statusCodes `r`n $_"
    }
}

Function Connect-WebrootUnity {
    [CmdletBinding()]
    param(
        [string]$client_id,
        [string]$client_secret,
        [string]$username,
        [string]$password,
        [string]$scope = '*',
        [Switch]$force
    )

    #Check that needed functions are loaded

    #Config file to save token info to
    $configDir = "$Env:AppData\WindowsPowerShell\Modules\WebrootUnity\0.1\Config.ps1xml"
    
    #If paramater was passed use as new token request
    if($client_id -or $client_secret -or $username -or $password){
        Write-Verbose "Paramaters passed, creating new request."
        Get-WebrootAuthToken -client_id $client_id -client_secret $client_secret -username $username -password $password -scope $scope
    }
    #No paramaters where passed use variable or config.
    else{
        #If there is no variable but a config file load
        if(!$WebrootAuthToken -and (Test-Path $configDir) -and !$Force){
            try {
                $global:WebrootAuthToken = Import-Clixml -Path $configDir -ErrorAction STOP
                if(!$WebrootAuthToken){
                    stop
                }
            }
            catch {
                Write-Warning "Corrupt Password file found, run Connect-WebrootUnity with -Force to fix this."
                BREAK
            }
            Write-Verbose 'Loaded config file.'
        }

        #Create or Renew the Token if it has expired or does not exist or forced
        if($WebrootAuthToken.expires -lt (Get-Date) -or !$WebrootAuthToken -or $Force){
            #If it can be renewed and not forced do so
            if($WebrootAuthToken.Renewable -gt (Get-Date) -and !$Force){
                Update-WebrootAuthToken -client_id $WebrootAuthToken.client_id -client_secret $WebrootAuthToken.client_secret -refresh_token $WebrootAuthToken.refresh_token     
            }
            #Request a new Token
            else{
                Get-WebrootAuthToken -client_id $WebrootAuthToken.client_id -client_secret $WebrootAuthToken.client_secret -username $WebrootAuthToken.username -password $WebrootAuthToken.password
            }
          
        }
        else{
            Write-Verbose "Token still valid."
        }

    }    

    #Create config file if not present.
    if (-not (Test-Path $configDir)){
        Write-Verbose "Creating config file."
        Write-Verbose $configDir
        New-item -Force -Path "$configDir" -ItemType File | Out-Null
    }

    #store the token
    #TODO secure token
    $WebrootAuthToken | Export-Clixml $configDir -Force
}

#endregion

#region -[SkyStatus]----------------------------------------------

function Get-WebrootEndpointStatus {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-status-site-keyCode_machineId_returnedInfo_modifiedSince_batchSize_continuation
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $KeyCode,
        $machineId,
        $returnedInfo,
        $modifiedSince,
        [int]$batchSize,
        $continuation,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/status/site/$($keyCode)?machineId=$($machineId)&returnedInfo=$($returnedInfo)&modifiedSince=$($modifiedSince)&batchSize=$($batchSize)&continuation=$($continuation)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
        if($All){
            while($Obj.ContinuationURI){
                Connect-WebrootUnity
                $Obj = Invoke-RestMethod -Method Get -uri $Obj.ContinuationURI -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
                $Obj.QueryResults
            }
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootEndpointStatusGSM {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $KeyCode,
        $machineId,
        $returnedInfo,
        $modifiedSince,
        [int]$batchSize,
        $continuation,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/status/gsm/$($KeyCode)?machineId=$($machineId)&returnedInfo=$($returnedInfo)&modifiedSince=$($modifiedSince)&batchSize=$($batchSize)&continuation=$($continuation)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
        if($All){
            while($Obj.ContinuationURI){
                Connect-WebrootUnity
                $Obj = Invoke-RestMethod -Method Get -uri $Obj.ContinuationURI -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
                $Obj.QueryResults
            }
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootKeycodeUsage {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-usage-site-keyCode_billingDate_continuation
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $KeyCode,
        $billingDate,
        $continuation
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/usage/site/$($keyCode)?billingDate=$($billingDate)&continuation=$($continuation)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootKeycodeUsageGSM {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-usage-site-keyCode_billingDate_continuation
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $KeyCode,
        $billingDate,
        $continuation,
        [switch]$All
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/usage/gsm/$($keyCode)?billingDate=$($billingDate)&continuation=$($continuation)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
        if($All){
            while($Obj.ContinuationURI){
                Connect-WebrootUnity
                $Obj = Invoke-RestMethod -Method Get -uri $Obj.ContinuationURI -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
                $Obj.QueryResults
            }
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

#endregion

#region -[Console]----------------------------------------------

## Console Access
function New-WebrootConsoleAdminRequest {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-access-gsm-gsmKey-addadminrequest
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $UserEmail,
        [Parameter(Mandatory=$True)]
        $ConfirmEmail
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/addadminrequest"
    
    $Body = @{UserEmail=$UserEmail;
              ConfirmEmail=$ConfirmEmail;}
    $Body = $Body | ConvertTo-Json


    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
        if($All){
            while($Obj.ContinuationURI){
                Connect-WebrootUnity
                $Obj = Invoke-RestMethod -Method Get -uri $Obj.ContinuationURI -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
                $Obj.QueryResults
            }
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleAdminRequest {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-access-gsm-gsmKey-addadminstatus_userEmail_confirmEmail
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $UserEmail,
        [Parameter(Mandatory=$True)]
        $ConfirmEmail
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/addadminstatus?userEmail=$($UserEmail)&confirmEmail=$($ConfirmEmail)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.QueryResults
        if($All){
            while($Obj.ContinuationURI){
                Connect-WebrootUnity
                $Obj = Invoke-RestMethod -Method Get -uri $Obj.ContinuationURI -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
                $Obj.QueryResults
            }
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

## Console GSM

### Site Management
function Get-WebrootConsoleGSMSiteList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-usage-site-keyCode_billingDate_continuation
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        $SiteID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function New-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteName,
        [Parameter(Mandatory=$True)]
        [int]$Seats,
        [Parameter(Mandatory=$True)]
        [string]$Comments,
        [ValidateSet("Annually","Quarterly","Monthly","Weekly")] 
        [string]$BillingCycle,
        [string]$BillingDate,
        [switch]$GlobalPolicies,
        [switch]$GlobalOverrides,
        [string]$PolicyId,
        [Parameter(Mandatory=$True)]
        [string]$Emails,
        [switch]$Trial


    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/sites"
    
    $Body = @{SiteName=$SiteName;
              Seats=$Seats;
              Comments=$Comments;
              BillingCycle=$BillingCycle;
              BillingDate=$BillingDate;
              GlobalPolicies=$GlobalPolicies;
              GlobalOverrides=$GlobalOverrides;
              PolicyId=$PolicyId;
              Emails=$Emails;
              Trial=$Trial;}
    $Body = $Body | ConvertTo-Json


    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Update-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/PUT-api-console-gsm-gsmKey-sites-siteId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$SiteName,
        [int]$Seats,
        [string]$Comments,
        [ValidateSet("Annually","Quarterly","Monthly","Weekly")] 
        [string]$BillingCycle,
        [string]$BillingDate,
        [switch]$GlobalPolicies,
        [switch]$GlobalOverrides,
        [string]$PolicyId,
        [string]$Emails
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)"
    
    $Body = @{}
    if($SiteName) {$Body.SiteName=$SiteName}
    if($Seats) {$Body.Seats=$Seats}
    if($Comments) {$Body.Comments=$Comments}
    if($BillingCycle) {$Body.BillingCycle=$BillingCycle}
    if($BillingDate) {$Body.BillingDate=$BillingDate}
    if($GlobalPolicies) {$Body.GlobalPolicies=$GlobalPolicies}
    if($GlobalOverrides) {$Body.GlobalOverrides=$GlobalOverrides}
    if($PolicyId) {$Body.PolicyId=$PolicyId}
    if($Emails) {$Body.Emails=$Emails}

    $Body = ConvertTo-Json $Body

    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }    
}

function Remove-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-deactivate
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteName
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/sites/$($SiteID)/deactivate"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }    
}

function Suspend-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-suspend
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteName
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/sites/$($SiteID)/suspend"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }    
}

function Resume-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-resume
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteName
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/sites/$($SiteID)/resume"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }    
}

function Convert-WebrootConsoleGSMSite {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-converttrial
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteName
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/access/gsm/$($GSMKey)/sites/$($SiteID)/converttrial"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }    
}

### User Management

function Get-WebrootConsoleGSMUserList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-admins
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/admins"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMUser {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-admins-userId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $UserID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/admins/$($UserID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj =Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Admins
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSiteUserList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-admins
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/admins"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSiteUser {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-admins-userId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID,
        [Parameter(Mandatory=$True)]
        $UserID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/admins/$($UserID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj =Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Admins
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Set-WebrootConsoleGSMSiteUser {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/PUT-api-console-gsm-gsmKey-sites-siteId-admins
#Can accept a csv of user ID's but only one access level
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$UserID,
        [Parameter(Mandatory=$True)]
        [ValidateSet("Full","Basic","None")] 
        [string]$AccessLevel
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/admins"
    
    switch ($AccessLevel){
        'Full'{$AccessLevel = 128}
        'Basic'{$AccessLevel = 1}
        'None'{$AccessLevel = 0}
    }

    $List = foreach($ID in $UserID){
        $object = New-Object –TypeName PSObject
        $object | Add-Member –MemberType NoteProperty –Name UserId -Value $ID
        $object | Add-Member –MemberType NoteProperty –Name AccessLevel -Value $AccessLevel
        Write-Output $object
    }

    $List = $(($List | ConvertTo-Json).trim('[]'))

    $Body = @"
{
    "Admins":  [
                   $List
               ]
}
"@

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

### Policy Management

function Get-WebrootConsoleGSMPolicyList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-policies
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/policies"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Policies
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMPolicy {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-policies-policyId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $PolicyID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/policies/$($PolicyID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Policies
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSitePolicyList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-policies
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/policies"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Policies
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSitePolicy {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-policies-policyId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID,
        [Parameter(Mandatory=$True)]
        $PolicyID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/policies/$($PolicyID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Policies
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

### Group Management

function Get-WebrootConsoleGSMSiteGroupList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Groups
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSiteGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups-groupId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        $GSMKey,
        [Parameter(Mandatory=$True)]
        $SiteID,
        [Parameter(Mandatory=$True)]
        $GroupID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($GroupID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Groups
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function New-WebrootConsoleGSMSiteGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-groups
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$GroupName,
        [Parameter(Mandatory=$True)]
        [string]$GroupDescription,
        [string]$PolicyId
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups"
    
    $Body = @{GroupName=$GroupName;
              GroupDescription=$GroupDescription;
              PolicyId=$PolicyId;}
    $Body = $Body | ConvertTo-Json


    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Set-WebrootConsoleGSMSiteGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-groups
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$GroupID,
        [ValidateSet("Unchanged","All","Update")] 
        [string]$Inheritance = "Unchanged",
        [string]$GroupName,
        [string]$GroupDescription,
        [string]$PolicyID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($GroupID)"

    switch ($Inheritance){
        'Unchanged'{$_Inheritance = 0}
        'All'{$_Inheritance = 1}
        'Update'{$_Inheritance = 3}
    }
    
    $Body = @{Inheritance=$_Inheritance;
              GroupName=$GroupName;
              GroupDescription=$GroupDescription;
              PolicyID=$PolicyID;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Remove-WebrootConsoleGSMSiteGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/DELETE-api-console-gsm-gsmKey-sites-siteId-groups-groupId_newGroupId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$GroupID,
        [string]$NewGroupID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($GroupID)?newGroupId=$($NewGroupID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Delete -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

### Endpoint Management

function Get-WebrootConsoleGSMSiteEndpointList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-endpoints_type_hostName_machineId_order_orderDirection_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteId,
        [string]$type,
        [string]$hostName,
        [string]$machineId,
        [string]$order,
        [string]$orderDirection,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/endpoints?type=$($type)&hostName=$($hostName)&machineId=$($machineId)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Endpoints
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/endpoints?type=$($type)&hostName=$($hostName)&machineId=$($machineId)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.Endpoints
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMSiteEndpoint {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups-groupId
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$EndpointID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/$($EndpointID)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.Endpoints
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMGroupEndpointList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups-groupId-endpoints_order_orderDirection_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteId,
        [string]$groupId,
        [string]$order,
        [string]$orderDirection,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/groups/$($groupId)/endpoints?order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Endpoints
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/endpoints?type=$($type)&hostName=$($hostName)&machineId=$($machineId)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.Endpoints
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Set-WebrootConsoleGSMEndpointGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/PUT-api-console-gsm-gsmKey-sites-siteId-endpoints
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointsList,
        [string]$SourceGroupId,
        [Parameter(Mandatory=$True)]
        [string]$TargetGroupId,
        [ValidateSet("Unchanged","All","Update")] 
        [string]$Inheritance = "Unchanged"
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints"

    switch ($Inheritance){
        'Unchanged'{$Inheritance = 0}
        'All'{$Inheritance = 1}
        'Update'{$Inheritance = 3}
    }
    
    $Body = @{EndpointsList=$EndpointsList;
              SourceGroupId=$SourceGroupId;
              TargetGroupId=$TargetGroupId;
              Inheritance=$Inheritance;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Set-WebrootConsoleGSMEndpointPolicy {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/PUT-api-console-gsm-gsmKey-sites-siteId-endpoints-policy
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointsList,
        [Parameter(Mandatory=$True)]
        [string]$PolicyId
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/policy"
    
    $Body = @{EndpointsList=$EndpointsList;
              PolicyId=$PolicyId;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Set-WebrootConsoleGSMGroupPolicy {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/PUT-api-console-gsm-gsmKey-sites-siteId-groups-groupId-endpoints-policy
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$GroupID,
        [Parameter(Mandatory=$True)]
        [string]$PolicyId
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($GroupID)/endpoints/policy"
    
    $Body = @{PolicyId=$PolicyId;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Put -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Reactivate-WebrootConsoleGSMEndpoint {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-endpoints-reactivate
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointsList
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/reactivate"
    
    $Body = @{EndpointsList=$EndpointsList;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Deactivate-WebrootConsoleGSMEndpoint {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-endpoints-deactivate
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointsList
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/deactivate"
    
    $Body = @{EndpointsList=$EndpointsList;}
    $Body = $Body | ConvertTo-Json

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Deactivate-WebrootConsoleGSMEndpointGroup {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-groups-groupId-endpoints-deactivate
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [Parameter(Mandatory=$True)]
        [string]$GroupID
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($GroupID)/endpoints/deactivate"    

    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

### Command Management

function Get-WebrootConsoleGSMCommandList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-commands_endpointId_command_commandState_startDate_endDate_order_orderDirection_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteId,
        [string]$endpointId,
        [string]$command,
        [string]$commandState,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$order,
        [string]$orderDirection,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/commands?endpointId=$($endpointId)&command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Commands
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/commands?endpointId=$($endpointId)&command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.Commands
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMGroupCommandList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups-groupId-commands_command_commandState_startDate_endDate_order_orderDirection_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteId,
        [string]$groupId,
        [string]$command,
        [string]$commandState,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$order,
        [string]$orderDirection,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/groups/$($groupId)/commands?command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Commands
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/groups/$($groupId)/commands?command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.Commands
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMEndpointCommandList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-endpoints-endpointId-commands_command_commandState_startDate_endDate_order_orderDirection_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteId,
        [string]$EndpointID,
        [string]$command,
        [string]$commandState,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$order,
        [string]$orderDirection,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/endpoints/$($EndpointID)/commands?command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.Commands
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteId)/endpoints/$($EndpointID)/commands?command=$($command)&commandState=$($commandState)&startDate=$($startDate)&endDate=$($endDate)&order=$($order)&orderDirection=$($orderDirection)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.Commands
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function New-WebrootConsoleGSMEndpointCommand {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-endpoints-commands
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointsList,
        [Parameter(Mandatory=$True)]
        [ValidateSet("scan","cleanup","uninstall","changekeycode","restart")]
        [string]$Command,
        [string]$Parameters
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/commands"
    
    $Body = @{EndpointsList=$EndpointsList;
              Command=$Command;
              Parameters=$Parameters;}
    $Body = $Body | ConvertTo-Json


    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function New-WebrootConsoleGSMEndpointGroupCommand  {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/POST-api-console-gsm-gsmKey-sites-siteId-groups-groupId-endpoints-commands
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$groupId,
        [Parameter(Mandatory=$True)]
        [ValidateSet("scan","cleanup","uninstall","changekeycode","restart")]
        [string]$Command,
        [string]$Parameters
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($groupId)/endpoints/commands"
    
    $Body = @{EndpointsList=$EndpointsList;
              Command=$Command;
              Parameters=$Parameters;}
    $Body = $Body | ConvertTo-Json


    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Post -Uri $url -ContentType "application/json" -Body $Body -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

### Threat History Information

function Get-WebrootConsoleGSMThreatList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-threathistory_startDate_endDate_returnedInfo_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$returnedInfo,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.ThreatRecords
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.ThreatRecords
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMGroupThreatList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-groups-groupId-threathistory_startDate_endDate_returnedInfo_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$GroupID,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$returnedInfo,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($groupId)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.ThreatRecords
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/groups/$($groupId)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.ThreatRecords
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootConsoleGSMEndpointThreatList {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-console-gsm-gsmKey-sites-siteId-endpoints-endpointId-threathistory_startDate_endDate_returnedInfo_pageSize_pageNr
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$True)]
        [string]$GSMKey,
        [Parameter(Mandatory=$True)]
        [string]$SiteID,
        [string]$EndpointID,
        [datetime]$startDate,
        [datetime]$endDate,
        [string]$returnedInfo,
        [int]$pageSize,
        [int]$pageNr,
        [switch]$All        
    )

    $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/$($EndpointID)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $Obj.ThreatRecords
        while($All -and ($($Obj.TotalAvailable) -gt ($Obj.PageNr * $Obj.PageSize))){
            $pageNr ++
            $url = "https://unityapi.webrootcloudav.com/service/api/console/gsm/$($GSMKey)/sites/$($SiteID)/endpoints/$($EndpointID)/threathistory?startDate=$($startDate)&endDate=$($endDate)&returnedInfo=$($returnedInfo)&pageSize=$($pageSize)&pageNr=$($pageNr)"    
            $Obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
            $Obj.ThreatRecords
        }
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

#endregion

#region HealthCheck

function Get-WebrootHealthCheckPing {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-health-ping
    [CmdletBinding()]
    param()

    $url = "https://unityapi.webrootcloudav.com/service/api/health/ping"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        Write-Output 'Success'
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

function Get-WebrootHealthCheckVersion {
#https://unityapi.webrootcloudav.com/Docs/APIDoc/Api/GET-api-health-version
    [CmdletBinding()]
    param()

    $url = "https://unityapi.webrootcloudav.com/service/api/health/version"
    
    Write-Verbose "Connecting"
    Connect-WebrootUnity
            
    try{
        $obj = Invoke-RestMethod -Method Get -Uri $url -ContentType "application/json" -Headers @{"Authorization" = "Bearer $($WebrootAuthToken.access_token)"}
        $obj.ServiceInformation
    }
    catch{
        Write-Error "Error: $($Error[0])"
    }
    
}

#endregion